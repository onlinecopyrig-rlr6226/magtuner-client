require "magtuner-client/version"
require "magtuner-client/base"
require "magtuner-client/client"
require "magtuner-client/user"
require "magtuner-client/configuration"

module MagtunerClient
  def self.configuration
    @configuration ||= Configuration.new
  end
  def self.configure
    yield(configuration)
  end

  def self.client_id
    return configuration.client_id unless configuration.client_id.nil?
    raise "Cannot find client_id in rails config. Add it to your initializer"
  end

  def self.client_secret
    return configuration.client_secret unless configuration.client_secret.nil?
    raise "Cannot find client_secret in rails config. Add it to your initializer"
  end

  def self.server_url
    return configuration.server_url unless configuration.server_url.nil?
    raise "Cannot find server_url in rails config. Add it to your initializer"
  end
end
