module MagtunerClient
  class Configuration
    attr_accessor :client_id, :client_secret, :server_url
  end
end
