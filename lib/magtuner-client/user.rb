module MagtunerClient
  module User
    # Check that all requirements in secrets and User are satisfied
    def self.included(klass)
      klass.extend(ClassMethods)
      if MagtunerClient.client_id.nil? || MagtunerClient.client_secret.nil? || MagtunerClient.server_url.nil?
        raise 'Secrets not configured properly, please provide oauth: {client_id: --, client_secret: --, app_url: --} or define such data in an initializer'
      end
      if defined? Rails
        if klass.table_exists?
          new_instance = klass.new
          missing_methods = []
          [:magtuner_authentication_token, :magtuner_authentication_token_secret].each do |m|
            begin
              new_instance.send m
            rescue NoMethodError
              missing_methods << m
            end
          end
          if missing_methods.any?
            Rails.logger.error "#{klass} instances shall provide #{missing_methods.join(', ')} methods. This is fine if you're just running migrations but MagtunerClient::User relyes on those methods"
          end
        end
      end
    end

    def has_valid_credentials?
      magtuner_authentication_token.present? || !magtuner_token_expired?
    end

    # Refresh and updates user profile
    def magtuner_profile!
      update_attribute :magtuner_profile, magtuner_client.me
      magtuner_profile
    end

    # Retruns a Magtuner::Client instance for the included model, relying on magtuner_authentication_token!
    def magtuner_client
      unless magtuner_authentication_token.blank?
        @magtuner_client ||= MagtunerClient::Client.new(magtuner_authentication_token!, MagtunerClient.server_url)
      end
    end

    # Check if authentication token is expired
    def magtuner_token_expired?
      magtuner_expires_at.nil? || (magtuner_expires_at < Time.now)
    end

    # Refresh the token if needed
    def magtuner_authentication_token!
      magtuner_refesh_token
      magtuner_authentication_token
    end

    # Refresh the token
    def magtuner_refesh_token
      if magtuner_token_expired? && magtuner_authentication_token_secret.present?
        response = RestClient.post "#{MagtunerClient.server_url}/oauth/token",
          grant_type: 'refresh_token',
          refresh_token: magtuner_authentication_token_secret,
          client_id: MagtunerClient.client_id,
          client_secret: MagtunerClient.client_secret

        oauth_response = JSON.parse(response.body)

        self.magtuner_authentication_token = oauth_response['access_token']
        self.magtuner_authentication_token_secret = oauth_response['refresh_token']
        self.magtuner_expires_at = oauth_response["expires_in"].to_i.seconds.since
        self.save
      end
    end

    module ClassMethods
      def magtuner_from_omniauth(auth)
        this_user = where(magtuner_uid: auth.uid).first_or_initialize do |user|
          random_psw = Devise.friendly_token[0,20]
          user.password = random_psw
        end

        this_user.magtuner_authentication_token = auth.credentials.token
        this_user.magtuner_authentication_token_secret = auth.credentials.refresh_token
        this_user.magtuner_expires_at = (Time.at(auth.credentials.expires_at) rescue nil)
        this_user.magtuner_profile = this_user.magtuner_client.me
        this_user.email = auth.info.email
        this_user.save
        this_user
      end
    end
  end
end
