module MagtunerClient
  class Client < Base
    def social_authentication_for?(social_network)
      self.me['authentications'].key? social_network.to_s
    end
    def get_twitter_uid
      self.me.fetch('authentications', {}).fetch('twitter', {}).fetch('uid', '')
    end

    def get_instagram_uid
      self.me.fetch('authentications', {}).fetch('instagram', {}).fetch('uid', '')
    end

    def get_facebook_token
      self.me.fetch('authentications', {}).fetch('facebook', {}).fetch('authentication_token', '')
    end

    def activity
      call :get, '/api/v1/users/activity.json'
    end

    def profile_completeness
      json = self.me

      return json["profile_completeness"] if json
    end
  end
end
