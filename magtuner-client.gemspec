# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'magtuner-client/version'

Gem::Specification.new do |spec|
  spec.name          = 'magtuner-client'
  spec.version       = MagtunerClient::VERSION
  spec.authors       = ['Opak Alex']
  spec.email         = ['opak.alexandr@gmail.com']
  spec.summary       = %{Magtuner client for oauth}
  spec.description   = %q{}
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_dependency 'rest-client'
end
